import processing.core.PApplet;

import java.util.Arrays;
import java.util.List;

interface Shape{
    public void move();
    public void draw(PApplet pApplet);
}

class Ball implements Shape{
    private int x;
    private int y;
    private int speed;

    public Ball(int x,int y,int speed){
        this.x=x;
        this.y=y;
        this.speed=speed;
    }

    @Override
    public void move() {
        x+=speed;
    }

    @Override
    public void draw(PApplet pApplet) {
        pApplet.ellipse(x,y,15,15); ;
    }

}

class Box implements Shape{
    private int x;
    private int y;
    private int speed;

    public Box(int x,int y,int speed){
        this.x=x;
        this.y=y;
        this.speed=speed;
    }

    @Override
    public void move() {
        x+=speed;
    }

    @Override
    public void draw(PApplet pApplet) {
        pApplet.rect(x,y,15,15); ;
    }

}

public class MovingShapes extends PApplet{

    public static final int WIDTH=620;
    public static final int HEIGHT=480;
    public static final int SPEED1=1;
    public static final int SPEED2=2;
    public static final int SPEED3=3;
    public static final int SPEED4=4;
    Ball ball1;
    Ball ball2;
    Box box;
    Ball ball4;
    private List<Shape> shapes;

    public static void main(String[] args) {
        PApplet.main("MovingShapes",args);
    }

    @Override
    public void settings() {
        size(HEIGHT,WIDTH);
    }

    @Override
    public void setup() {
        ball1= new Ball(0,HEIGHT*1/4,SPEED1);
        ball2= new Ball(0,HEIGHT*2/4,SPEED2);
        box= new Box(0,HEIGHT*3/4,SPEED3);
        ball4= new Ball(0,HEIGHT*4/4,SPEED4);
        shapes= Arrays.asList(ball1,ball2,box,ball4);

    }

    @Override
    public void draw() {
        shapes.forEach(Shape::move);
        shapes.forEach(shape->shape.draw(this));
    }
}
